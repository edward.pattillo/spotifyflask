import spotipy.util as util

class Config:

    def __init__(self):
        self.dbName = "database/database.db"
        self.secret_key = "yourSecretKey"
        self.token = util.prompt_for_user_token(
            username="",
            client_id="",
            client_secret="",
            scope="playlist-read-private",
            redirect_uri="http://localhost:8888/callback")



        