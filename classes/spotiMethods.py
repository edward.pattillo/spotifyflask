
import spotipy 
from config import Config 

# create a config object so we can access it's properties
config = Config()
token = config.token

class SpotifyMethods:

    # this is a contructor class
    # it holds properties of the object that can be 
    # referred to throughout via "self"
    def __init__(self):
        # put properties here
        # self.dbPath = config.dbName
        self.spotify = spotipy.Spotify(auth=token)
    
# -----------------------------------------------


    def getArtistImage(self,artistName):

        spotify = self.spotify

        # returns dictionary of results
        results = spotify.search(q='artist:' + artistName, type='artist')
        # print(results)

        if len(results['artists']['items']) > 0:

            image = results['artists']['items'][0]['images'][0]['url']
        else:
            image = False
        return image 


# -----------------------------------------------

    def getCurrent(self):

        spotify = self.spotify
        # info = spotify.current_user_playlists()
        info = spotify.user_playlist_tracks(user="nityangi",playlist_id="1s1wy1OJqakVfnTsG8xWjN",fields="items",limit=50, offset=0,market="NZ")

        for track in info['items']:
            trackInfo = track

        return trackInfo
 