# import flash application
from flask import Flask, render_template, request, redirect, url_for, flash, session, logging
# import our config file config.py
from config import Config
# import functools
from functools import wraps 
# import our classes in the classes folder
from classes.user import User
import classes.forms
from classes.spotiMethods import SpotifyMethods



# ---------------------------------------------
 
# create an object with our Config class properties
config = Config()

# instantiate flask
app = Flask(__name__)

# set the secret key for encryption
app.secret_key = config.secret_key
 
spotifyMethods = SpotifyMethods()

# ---------------------------------------------

# checks if user is logged in
def is_loggged_in(f):
    @wraps(f)
    # We use *args and **kwargs because we don't know how many
    # parameters (arguements) will be passed to each function
    # that we're testing.
    def wrap(*args,**kwargs):
        # if the user is logged in then it's business as usual
        if 'logged_in' in session:
            return f(*args,**kwargs)
        else:
            # if the user isn't logged in then flash them a message
            # and boot them out to the login page.
            flash("Unauthorised access. Please log in.","danger")
            return redirect(url_for('login'))
    return wrap

# ---------------------------------------------

# this route defines the behavior at the root endpoint of our site
@app.route("/", methods=["GET", "POST"])
def home():

    form = classes.forms.GetArtistImage(request.form)
    defaultImage = "https://i.imgur.com/pWROHf2.jpg"

    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        artistName = form.artistName.data 

        image = spotifyMethods.getArtistImage(artistName)
        
        if image:
 
            return render_template("home.html",form=form,image=image)
        else:
            flash("That artist isn't in Spotify. Check your spelling.","danger")
            return render_template("home.html",form=form,image=defaultImage)
    else: 

        return render_template("home.html",form=form,image=defaultImage)
             
# ---------------------------------------------


# this route is for the 'playing' endpoint
@app.route("/playing")
def playing():

    info = spotifyMethods.getCurrent()


    # this reaches into our template folder and renders the playing.html page
    return render_template("playing.html",info=info)

# ---------------------------------------------


# this is a user dashboard for our app. we pass it a name as appended to the url
# example /user/Mick ... "Mick" will become the variable "name" with data type string
@app.route("/user/<string:name>")
# this will only be available to logged in users
@is_loggged_in
def user(name):
    return render_template("user.html", name=name)

# ---------------------------------------------


# this handles the registration endpoint
@app.route("/register", methods=["GET", "POST"])
def register():

    # this creates an object that contains the RegisterForm fields
    # and validation rules
    form = classes.forms.RegisterForm(request.form)

    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        username = form.username.data
        email = form.email.data
        password = form.password.data

        # create a user object so that we can access the user methods
        # this comes from classes/user.py
        user = User()

        # here we call the insertUser method on the user class
        # it will return a boolean of either True or False
        success = user.insertUser(username,password,email)

        # if success:   is the same thing as   if success == True
        if success:
            # if user successfully registers we let them know and send them to login
            flash("You have now registered.","success")
            return redirect(url_for("login"))
        else:
            # if insertUser returns False then notify and let them try again
            flash("This user already exists. Try again.","danger")
            return redirect(url_for("register"))

    # this is what happens when somone just visits the register
    # enpoint for the first time (before they submit something)
    return render_template("register.html", form=form)


# ---------------------------------------------


# this route handles the login endpoint
@app.route("/login", methods=["GET", "POST"])
def login():
    # this creates an object that contains the 
    # LoginForm fields and validation rules
    form = classes.forms.LoginForm(request.form)

    # because this form submits to itself we will see if 
    # anything has been submitted and treat it differently
    # this only works if all fields have validated correctly
    if request.method == "POST" and form.validate():

        # grab user inputs from form 
        username = form.username.data
        passwordAttempt = form.password.data

        # create a user object so that we can access the 
        # user methods this comes from classes/user.py
        user = User()

        # here we call the authenticateUser method on the user class
        # it will return a boolean of either True or False
        auth = user.authenticateUser(username, passwordAttempt)

        # if authentication was successful
        if auth:
            # create a user session. session comes from flask
            session['logged_in'] = True
            session['username'] = username

            # notify user and send them to their dashboard
            flash("You are now logged in, "+username+".","success")
            return redirect(url_for("user", name=username))
        
        else:
            # if authentication failed 
            error = "Incorrect username or password."
            return render_template("login.html",form=form,error=error)

    # this is what happens when somone just visits the login
    # enpoint for the first time (before they submit something)
    return render_template("login.html",form=form)

# ---------------------------------------------



# this is the logout endpoint
@app.route("/logout")
# this will only be available to logged in users
@is_loggged_in
def logout():

    # this gets rid of the current session (logs user out)
    session.clear()

    # notify and send them to the login endpoint
    flash("You are now logged out.","success")
    return redirect(url_for("login"))

# ---------------------------------------------

# endpoint for custom 404 error page
@app.errorhandler(404)
def page_not_found(e):
    # log the error message to the terminal
    app.logger.info(e)
    # send the user to our custom error page
    return render_template("errors/404.html")

# ---------------------------------------------

if __name__ == "__main__":
    # finally, this runs our app
    # to get rid of debug mode just set it to False or remove it
    app.run(debug=True)